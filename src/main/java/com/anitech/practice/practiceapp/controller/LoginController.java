package com.anitech.practice.practiceapp.controller;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anitech.practice.practiceapp.model.UserProfile;

@RestController
@RequestMapping("/rest/api")
public class LoginController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@CrossOrigin
	@PostMapping("/loginUser")
	public Map<String,String> loginUser(@RequestBody UserProfile user) {
		logger.info("entered into login user function "+user);
		Map<String , String> map = new HashMap<String, String>();
		if("ankur@gmail.com".equals(user.getEmail()) && "akkijava".equals(user.getPassword()))  {
			map.put("message","success");
		}else {
			map.put("message","failed");
		}
		
		return map;
	}
	
	
}
