package com.anitech.practice.practiceapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.buf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.anitech.practice.practiceapp.model.Country;
import com.anitech.practice.practiceapp.model.State;

@RestController
@RequestMapping("/rest")
public class DemoController {

	private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

	@CrossOrigin
	@GetMapping("/getcountries")
	public List<Country> getCountries(){
		
		logger.info("entered into DemoController....");
		List<Country> listcountries = new ArrayList();
		
		Country country = new Country(0,"SELECT COUNTRY");
		Country country1 = new Country(1,"INDIA");
		Country country2 = new Country(2,"NEW ZELAND");
		Country country3 = new Country(3,"AUSTRALIA");
		Country country4 = new Country(4,"USA");
		Country country5 = new Country(5,"CANADA");
		Country country6 = new Country(6,"PAKISTAN");
		
		listcountries.add(country);
		listcountries.add(country1);
		listcountries.add(country2);
		listcountries.add(country3);
		listcountries.add(country4);
		listcountries.add(country5);
		listcountries.add(country6);
		
		
		return listcountries;
		
	}
	
	@CrossOrigin
	@GetMapping("/getstates/{country}")
	public List<State> getStates(@PathVariable(name="country") String country){
		
		logger.info("selected country ::"+country);
		
		List<State> stateList = new ArrayList();
			
			if("INDIA".equalsIgnoreCase(country)) {
				State state = new State(0, "SELECT SATE");
				State state1 = new State(1, "TELANGANA");
				State state2 = new State(2, "ANDHRA PRADESH");
				State state3 = new State(3, "HIMACHAL PRADESH");
				State state4 = new State(4, "TAMIL NADU");
				State state5 = new State(5, "MADHYA PRADESH");
				
				stateList.add(state);
				stateList.add(state1);
				stateList.add(state2);
				stateList.add(state3);
				stateList.add(state4);
				stateList.add(state5);
				
				
			}else if("USA".equalsIgnoreCase(country)) {
				
				State state = new State(0, "SELECT SATE");
				State state1 = new State(1, "CALIFORNIA");
				State state2 = new State(2, "TEXAS");
				State state3 = new State(3, "FLORIDA");
				State state4 = new State(4, "NEW JERSEY");
				State state5 = new State(5, "NEW YORK");
				
				stateList.add(state);
				stateList.add(state1);
				stateList.add(state2);
				stateList.add(state3);
				stateList.add(state4);
				stateList.add(state5);
				
				
			}else {
				logger.info("No Record found");
				return stateList;
			}
			logger.info("Records ::"+stateList);
		
		return stateList;
		
	}
	
}
